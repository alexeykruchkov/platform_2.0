// navigation
(function() {
	var link = document.querySelector(".mobile-nav");
	var body= document.querySelector("body");

	link.addEventListener("click", function(event) {
		event.preventDefault();
		body.classList.toggle("mob-nav-active");
	});
})();

$(window).scroll(function () {
		if( $(window).scrollTop() > 20 && !($('.main-header__nav').hasClass('header-sticky'))){
			$('.top').addClass('header-sticky');
		} else if ($(window).scrollTop() < 30){
			$('.top').removeClass('header-sticky');
		}
});
// try
function checkForm() {
	var ok = true;
	$('.try-form__input').each(function(){
	if ($.trim($(this).val()) == '') {
		ok = false;
	}
})
	console.log(ok);
	return ok;
}